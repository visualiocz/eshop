package cz.brezinajn.eshop.dao.arrow

import arrow.core.Either
import cz.brezinajn.eshop.dao.ProductService


class ProductRepository(private val productService: ProductService) {

    suspend fun listProducts() = Either.catch { productService.listProducts() }
    suspend fun order(id: String, quantity: Int) = Either.catch {
        productService.order(id, quantity)
    }
}


package cz.brezinajn.eshop.dao.retrofit

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import cz.brezinajn.eshop.model.Product
import okhttp3.ResponseBody
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ProductService {

    @GET("/products")
    suspend fun listProducts(): List<Product>


    @POST("/order")
    suspend fun order(@Body body: OrderReq): ResponseBody
}

data class OrderReq(
    val id: String,
    val quantity: Int
)



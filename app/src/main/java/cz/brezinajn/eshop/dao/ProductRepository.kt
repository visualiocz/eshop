package cz.brezinajn.eshop.dao


object ProductRepository {
    suspend fun listObjects() = ProductService.listProducts()
    suspend fun order(id: String, quantity: Int) = ProductService.order(id, quantity)
}
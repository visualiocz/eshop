package cz.brezinajn.eshop.dao

import cz.brezinajn.eshop.model.Product
import kotlinx.coroutines.delay

object ProductService {

     private val products = listOf(
        Product(
            id = "1",
            title = "Test",
            content = "A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh! A když uvalím svou mstu na tebe, seznáš, že jméno mé je Bůh!",
            image = "https://images.sftcdn.net/images/t_app-cover-l,f_auto/p/befbcde0-9b36-11e6-95b9-00163ed833e7/260663710/the-test-fun-for-friends-screenshot.jpg",
            price = 250.0
        ),
         Product(
             id = "2",
             title = "Test",
             content = "Test",
             image = "https://ichef.bbci.co.uk/news/660/cpsprodpb/37B5/production/_89716241_thinkstockphotos-523060154.jpg",
             price = 250.0
         ),
         Product(
             id = "2",
             title = "Test",
             content = "Test",
             image = "https://ichef.bbci.co.uk/news/660/cpsprodpb/37B5/production/_89716241_thinkstockphotos-523060154.jpg",
             price = 250.0
         ),
         Product(
             id = "2",
             title = "Test",
             content = "Test",
             image = "https://ichef.bbci.co.uk/news/660/cpsprodpb/37B5/production/_89716241_thinkstockphotos-523060154.jpg",
             price = 250.0
         ),
         Product(
             id = "2",
             title = "Test",
             content = "Test",
             image = "https://ichef.bbci.co.uk/news/660/cpsprodpb/37B5/production/_89716241_thinkstockphotos-523060154.jpg",
             price = 250.0
         ),
         Product(
             id = "2",
             title = "Test",
             content = "Test",
             image = "https://ichef.bbci.co.uk/news/660/cpsprodpb/37B5/production/_89716241_thinkstockphotos-523060154.jpg",
             price = 250.0
         ),
         Product(
             id = "2",
             title = "Test",
             content = "Test",
             image = "https://ichef.bbci.co.uk/news/660/cpsprodpb/37B5/production/_89716241_thinkstockphotos-523060154.jpg",
             price = 250.0
         ),
         Product(
             id = "2",
             title = "Test",
             content = "Test",
             image = "https://ichef.bbci.co.uk/news/660/cpsprodpb/37B5/production/_89716241_thinkstockphotos-523060154.jpg",
             price = 250.0
         )
    )

    suspend fun listProducts(): List<Product> {
        delay(1500)
        return products
    }

    suspend fun order(id: String, quantity: Int) {
        delay(500)
        if(quantity <= 0) throw IllegalStateException("Quantity must be positive")
    }
}
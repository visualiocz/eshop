package cz.brezinajn.eshop.dao.retrofit

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object ProductRepository{

    private const val baseUrl = "www.someurl.com"
    private val service = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
        .create(ProductService::class.java)

    suspend fun listProducts() = service.listProducts()

    suspend fun order(id: String, quantity: Int) = service.order(
        OrderReq(id, quantity)
    )
}
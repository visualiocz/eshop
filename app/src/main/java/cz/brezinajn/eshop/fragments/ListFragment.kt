package cz.brezinajn.eshop.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import cz.brezinajn.eshop.R
import cz.brezinajn.eshop.adapters.RecyclerViewAdapter
import cz.brezinajn.eshop.util.Failure
import cz.brezinajn.eshop.util.Init
import cz.brezinajn.eshop.util.Loading
import cz.brezinajn.eshop.util.Success
import cz.brezinajn.eshop.viewholders.ProductViewHolder
import cz.brezinajn.eshop.viewmodels.MainActivityState
import cz.brezinajn.eshop.viewmodels.MainActivityViewModel
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : Fragment() {

    private val adapter by lazy {
        RecyclerViewAdapter(
            layout = R.layout.item_product,
            viewHolderFactory = ::ProductViewHolder,
            onItemClicked = {
                navigateToDetail(it.id)
            }
        )
    }

    private val viewModel by lazy {
        ViewModelProvider(this.activity!!)[MainActivityViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler.setHasFixedSize(true)
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = adapter


        viewModel.liveData.observe(this, Observer { updateView(it) })
    }

    private fun updateView(state: MainActivityState) {
        when (val products = state.products) {
            Init -> {
            }
            Loading -> {
            }
            is Success -> adapter.update(products.value)
            is Failure -> {
            }
        }
    }

    private fun navigateToDetail(id: String) {
        val direction = ListFragmentDirections.actionListFragmentToDetailFragment(id)
        findNavController().navigate(direction)
    }
}

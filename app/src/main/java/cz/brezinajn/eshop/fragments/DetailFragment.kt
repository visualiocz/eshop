package cz.brezinajn.eshop.fragments


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import cz.brezinajn.eshop.R
import cz.brezinajn.eshop.util.Failure
import cz.brezinajn.eshop.util.GlideApp
import cz.brezinajn.eshop.util.Init
import cz.brezinajn.eshop.util.Success
import cz.brezinajn.eshop.viewmodels.DetailFragmentState
import cz.brezinajn.eshop.viewmodels.DetailFragmentViewModel
import cz.brezinajn.eshop.viewmodels.MainActivityViewModel
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.coroutines.launch


class DetailFragment : Fragment() {

    private val activityViewModel by lazy {
        ViewModelProvider(this.activity!!)[MainActivityViewModel::class.java]
    }

    private val viewModel by lazy {
        ViewModelProvider(this)[DetailFragmentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_detail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setHasOptionsMenu(true)

        DetailFragmentArgs.fromBundle(requireArguments()).productId
            .let(activityViewModel::getProductById)
            ?.let(viewModel::setProduct)
            ?: throw IllegalStateException("Object cannot be empty.")


        viewModel.liveData.observe(this, Observer {
            updateView(it)
        })


        quantity.addTextChangedListener {
            viewModel.setQuantity(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.order, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> activity?.onBackPressed()
            R.id.action_order ->
                if (viewModel.state.orderState is Init || viewModel.state.orderState is Failure)
                    lifecycleScope.launch {
                        when (val orderState = viewModel.order()) {
                            is Success -> {
                                Toast.makeText(context!!, getString(R.string.order_success), Toast.LENGTH_LONG).show()
                                activity?.onBackPressed()
                            }
                            is Failure -> showError(orderState.exception.message)
                        }
                    }

        }

        return super.onOptionsItemSelected(item)
    }

    private fun updateView(state: DetailFragmentState) {
        state.product ?: return

        GlideApp.with(this)
            .load(state.product.image)
            .fitCenter()
            .into(image)

        title.text = state.product.title
        content.text = state.product.content
    }

    fun showError(message: String?) {
        Snackbar.make(scrollView, message ?: getString(R.string.error_generic), Snackbar.LENGTH_LONG).show()
    }
}


fun TextInputEditText.addTextChangedListener(block: (String) -> Unit): TextWatcher {
    val watcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            block(s.toString())
        }
    }

    addTextChangedListener(watcher)
    return watcher
}
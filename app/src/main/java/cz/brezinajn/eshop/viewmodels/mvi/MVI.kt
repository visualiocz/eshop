package cz.brezinajn.eshop.viewmodels.mvi


import arrow.Kind
import arrow.core.Either
import arrow.fx.typeclasses.Concurrent
import cz.brezinajn.eshop.viewmodels.LiveDataViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.consumeEach

typealias Await<F, G> = suspend Kind<F, G>.() -> Either<Throwable, G>
typealias Reducer<ACTION, STATE> = (ACTION, STATE) -> STATE
typealias GetEffect<F, ACTION, STATE> = Concurrent<F>.(action: ACTION, state: STATE, dispatch: Dispatch<ACTION>) -> Kind<F, Unit>
typealias Dispatch<ACTION> = (ACTION) -> Unit

@ExperimentalCoroutinesApi
@ObsoleteCoroutinesApi
open class MVI<F, STATE, ACTION>(
    initialState: STATE,
    private val concurrent: Concurrent<F>,
    val await: Await<F, *>,
    coroutineScope: CoroutineScope,
    private val reducer: Reducer<ACTION, STATE>,
    private val getEffect: GetEffect<F, ACTION, STATE>
) : LiveDataViewModel<STATE>(initialState) {
    fun dispatch(action: ACTION) {
        dispatchActor.offer(action)
    }

    // actor used to synchronize global state writing
    private val dispatchActor = coroutineScope.actor<ACTION>(capacity = Channel.BUFFERED) {
        channel.consumeEach { action -> update(action).await() }
    }

    private fun update(action: ACTION) = concurrent.fx.concurrent {
        val newState = reducer(action, state)
        effect { setState(newState) }.bind()
        getEffect(action, newState, ::dispatch).fork().bind()
    }
}
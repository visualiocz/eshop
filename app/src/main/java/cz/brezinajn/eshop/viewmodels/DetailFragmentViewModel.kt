package cz.brezinajn.eshop.viewmodels

import cz.brezinajn.eshop.dao.ProductRepository
import cz.brezinajn.eshop.model.Product
import cz.brezinajn.eshop.util.*

class DetailFragmentViewModel : LiveDataViewModel<DetailFragmentState>(DetailFragmentState()) {
    suspend fun order(): NetworkStateMachine<Unit> {
        val product = state.product ?: throw IllegalStateException("Product cannot be null")

        setState(state.copy(orderState = Loading))

        try {
            ProductRepository.order(
                id = product.id,
                quantity = state.quantity
            ).let(::Success)
        } catch (e: Exception) {
            Failure(e)
        }.let { state.copy(orderState = it) }
            .let(::setState)

        return state.orderState
    }

    fun setProduct(product: Product) {
        setState(state.copy(product = product))
    }

    fun setQuantity(quantity: String) {
        val quantityInt = quantity.toIntOrNull() ?: return
        if (quantityInt != state.quantity)
            state.copy(quantity = quantityInt).let(::setState)
    }
}

data class DetailFragmentState(
    val quantity: Int = 0,
    val product: Product? = null,
    val orderState: NetworkStateMachine<Unit> = Init
)
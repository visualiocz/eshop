package cz.brezinajn.eshop.viewmodels.mvi

import arrow.Kind
import arrow.core.Either
import arrow.fx.typeclasses.Concurrent
import arrow.syntax.function.partially5
import cz.brezinajn.eshop.dao.arrow.ProductRepository
import cz.brezinajn.eshop.model.Product
import cz.brezinajn.eshop.util.*
import cz.brezinajn.eshop.viewmodels.MainActivityState
import cz.brezinajn.eshop.viewmodels.products
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi


sealed class MainActivityAction {
    internal data class SetProducts(val products: NetworkStateMachine<List<Product>>): MainActivityAction()
    object LoadProducts : MainActivityAction()
}

internal fun reducer(action: MainActivityAction, state: MainActivityState): MainActivityState =
    when (action) {
        is MainActivityAction.SetProducts -> MainActivityState.products.set(state, action.products)
        MainActivityAction.LoadProducts -> state
    }

private data class MainActivityDependencies(val productRepository: ProductRepository)

@Suppress("UNUSED_PARAMETER")
private fun <F> Concurrent<F>.getEffect(
    action: MainActivityAction,
    state: MainActivityState,
    dispatch: Dispatch<MainActivityAction>,
    dependencies: MainActivityDependencies
): Kind<F, Unit> = when (action) {
    MainActivityAction.LoadProducts -> loadProducts(dependencies.productRepository::listProducts, dispatch)
    is MainActivityAction.SetProducts -> unit()
}

internal fun <F> Concurrent<F>.loadProducts(
    listProducts: suspend () -> Either<Throwable, List<Product>>,
    dispatch: Dispatch<MainActivityAction.SetProducts>
) = fx.concurrent {
    dispatch(
        MainActivityAction.SetProducts(
            Loading
        )
    )

    val productsResult = effect { listProducts() }
        .bind()

    productsResult.toNetworkStateMachine()
        .let(MainActivityAction::SetProducts)
        .let(dispatch)
}

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
class MainActivityMVI<F> (
    await: Await<F, *>,
    concurrent: Concurrent<F>,
    coroutineScope: CoroutineScope,
    productRepository: ProductRepository
): MVI<F, MainActivityState, MainActivityAction>(
    initialState = MainActivityState(),
    await = await,
    concurrent = concurrent,
    coroutineScope = coroutineScope,
    reducer = ::reducer,
    getEffect = Concurrent<F>::getEffect.partially5(
        MainActivityDependencies(
            productRepository
        )
    )
)
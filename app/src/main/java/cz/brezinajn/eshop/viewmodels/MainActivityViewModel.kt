package cz.brezinajn.eshop.viewmodels

import androidx.lifecycle.viewModelScope
import arrow.optics.optics
import cz.brezinajn.eshop.dao.ProductRepository
import cz.brezinajn.eshop.model.Product
import cz.brezinajn.eshop.util.*
import kotlinx.coroutines.launch

class MainActivityViewModel : LiveDataViewModel<MainActivityState>(MainActivityState()) {
    fun loadData() {
        state.copy(
            products = Loading
        ).let(::setState)

        viewModelScope.launch {
            try {
                Success(ProductRepository.listObjects())
            } catch (e: Exception) {
                Failure(e)
            }.let { state.copy(products = it) }
                .let(::setState)
        }
    }

    fun getProductById(id: String): Product? = when (val products = state.products) {
        Init, Loading, is Failure -> null
        is Success -> products.value.find { it.id == id }
    }
}


@optics
data class MainActivityState(
    val products: NetworkStateMachine<List<Product>> = Init
    ) {
    companion object
}
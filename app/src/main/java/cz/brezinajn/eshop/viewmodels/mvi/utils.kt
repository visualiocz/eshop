package cz.brezinajn.eshop.viewmodels.mvi

import arrow.core.Either
import arrow.fx.IOOf
import arrow.fx.fix
import cz.brezinajn.eshop.util.Failure
import cz.brezinajn.eshop.util.NetworkStateMachine
import cz.brezinajn.eshop.util.Success
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

suspend fun <A> IOOf<A>.await(): Either<Throwable, A> = suspendCancellableCoroutine { continuation ->
    val disposableHandle = fix().unsafeRunAsyncCancellable { continuation.resume(it) }
    continuation.invokeOnCancellation { disposableHandle.invoke() }
}

fun <F>Either<Throwable,F>.toNetworkStateMachine(): NetworkStateMachine<F> = fold(
    ifLeft = ::Failure,
    ifRight = ::Success
)
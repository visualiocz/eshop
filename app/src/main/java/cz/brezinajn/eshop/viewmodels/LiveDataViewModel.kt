package cz.brezinajn.eshop.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlin.properties.Delegates

abstract class LiveDataViewModel<T>(initialState: T) : ViewModel(){
    private var privateState: T by Delegates.observable(initialState) { _, old, new ->
        if (old !== new)
            mutableLiveData.postValue(new)
    }
    val state: T
        get() = privateState

    protected fun setState(newState: T){
        privateState = newState
    }

    private val mutableLiveData: MutableLiveData<T> = MutableLiveData()
    val liveData: LiveData<T>
        get() = mutableLiveData

}
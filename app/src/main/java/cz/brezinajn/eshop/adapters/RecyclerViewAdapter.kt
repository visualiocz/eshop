package cz.brezinajn.eshop.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.brezinajn.eshop.viewholders.RecyclerViewHolder
import kotlin.properties.Delegates

typealias OnItemClick<T> = (element: T) -> Unit

class RecyclerViewAdapter<VH : RecyclerViewHolder<T>, T>(
    private val layout: Int,
    private val viewHolderFactory: (View) -> VH,
    private val onItemClicked: OnItemClick<T>? = null
) : RecyclerView.Adapter<VH>() {

    private var data by Delegates.observable(emptyList<T>()){_, old, new ->
        if (new !== old) notifyDataSetChanged()
    }

    fun update(newData: List<T>) {
        data = newData
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH =
        LayoutInflater.from(parent.context)
            .inflate(layout, parent, false)
            .let(viewHolderFactory)

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bindModel(data[position])
        if (onItemClicked != null)
            holder.itemView.setOnClickListener { onItemClicked.invoke(data[position]) }
    }
}
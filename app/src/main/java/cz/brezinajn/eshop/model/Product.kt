package cz.brezinajn.eshop.model

data class Product(
    val id: String,
    val title: String,
    val content: String,
    val image: String,
    val price: Double
)
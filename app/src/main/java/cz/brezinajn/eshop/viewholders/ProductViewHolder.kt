package cz.brezinajn.eshop.viewholders

import android.view.View
import com.bumptech.glide.Glide
import cz.brezinajn.eshop.model.Product
import cz.brezinajn.eshop.util.GlideApp
import kotlinx.android.synthetic.main.item_product.view.*

class ProductViewHolder(itemView: View) : RecyclerViewHolder<Product>(itemView) {
    override fun bindModel(model: Product) {
        itemView.tvTitle.text = model.title
        itemView.tvContent.text = model.content.takeWords(15)

        GlideApp.with(itemView)
            .load(model.image)
            .fitCenter()
            .into(itemView.titleImage)
    }
}


fun String.takeWords(count: Int): String {
   val split = this.split(" ")
    return if(split.size <= count)  this
    else split.take(count).joinToString(separator = " ", postfix = "…")
}
package cz.brezinajn.eshop.util

sealed class NetworkStateMachine<out T>
object Init : NetworkStateMachine<Nothing>()
object Loading : NetworkStateMachine<Nothing>()
data class Success<T>(val value: T) : NetworkStateMachine<T>()
data class Failure(val exception: Throwable) : NetworkStateMachine<Nothing>()
package cz.brezinajn.eshop.viewmodels.mvi

import arrow.core.right
import arrow.fx.IO
import arrow.fx.extensions.io.concurrent.concurrent
import cz.brezinajn.eshop.model.Product
import cz.brezinajn.eshop.util.Loading
import cz.brezinajn.eshop.util.Success
import cz.brezinajn.eshop.viewmodels.MainActivityState
import io.mockk.spyk
import io.mockk.verifySequence
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test

class MainActivityMVIKtTest {

    private val products = listOf(
        Product(
            id = "",
            content = "",
            image = "",
            price = 250.0,
            title = ""
        )
    )

    @Test
    fun reducerTest() {
        val originalState = MainActivityState()

        val args = Success(products)

        val newState = reducer(
            action = MainActivityAction.SetProducts(args),
            state = originalState
        )

        assertEquals(newState, originalState.copy(products = args))

        val newState2 = reducer(MainActivityAction.SetProducts(Loading), originalState)

        assertEquals(newState2, originalState.copy(products = Loading))
    }

    @Test
    fun loadProductsTest() {
        val dispatch = spyk<Dispatch<MainActivityAction.SetProducts>>()
        val listProducts = suspend { products.right() }
        runBlocking {
            IO.concurrent().loadProducts(
                dispatch = dispatch,
                listProducts = listProducts
            ).await()
        }

        verifySequence{
            dispatch.invoke(MainActivityAction.SetProducts(Loading))
            dispatch.invoke(MainActivityAction.SetProducts(Success(products)))
        }
    }
}